package kz.kaspi.bookshell.controller;


import kz.kaspi.bookshell.dto.BookFilter;
import kz.kaspi.bookshell.entities.Books;
import kz.kaspi.bookshell.entities.Genres;
import kz.kaspi.bookshell.entities.Languages;
import kz.kaspi.bookshell.repositories.UserRepository;
import kz.kaspi.bookshell.services.BooksService;
import kz.kaspi.bookshell.services.GenreService;
import kz.kaspi.bookshell.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/books")
@PreAuthorize("hasAuthority('USER')")
public class BooksController {

    @Autowired
    private BooksService service;

    @Autowired
    private GenreService genresService;

    @Autowired
    private LanguageService lngService;

    @ModelAttribute("allGenres")
    public List<Genres> allGenres() {
        return genresService.findAll();
    }

    @ModelAttribute("allLanguages")
    public List<Languages> allLanguages() {
        return lngService.findAll();
    }

    @GetMapping
    public String index(Model model, @Param("keyword") String keyword, @ModelAttribute(name = "filterObjects") BookFilter filter, Principal principal) {
        List<Books> booksList = service.findAll(filter.getOrderBy(),
                filter.getMode(),
                filter.getPriceFrom(),
                filter.getPriceTo(),
                filter.getGenreId(),
                filter.getLanguageId(),
                keyword
                );
        model.addAttribute("orderby", filter.getOrderBy());
        model.addAttribute("mode", toggleMode(filter.getMode()));

        model.addAttribute("allBooks", booksList);
        model.addAttribute("allLanguages", lngService.findAll());


        return "book-list";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable(value = "id") Long id, Model model) {
        Books book = service.getOne(id);
        System.out.println(book);
        model.addAttribute("book", book);
        return "view";
    }


    //____________________________________________


    private String toggleMode(String mode){
        String newMode = "desc";
        if(mode.equals("desc"))
            newMode="asc";
        return newMode;
    }


}
