package kz.kaspi.bookshell.controller;


import kz.kaspi.bookshell.dto.BookDTO;
import kz.kaspi.bookshell.dto.BookFilter;
import kz.kaspi.bookshell.entities.Books;
import kz.kaspi.bookshell.entities.Genres;
import kz.kaspi.bookshell.entities.Languages;
import kz.kaspi.bookshell.repositories.UserRepository;
import kz.kaspi.bookshell.services.BooksService;
import kz.kaspi.bookshell.services.GenreService;
import kz.kaspi.bookshell.services.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/admin/books")
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminBooksController {

    @Autowired
    private BooksService service;

    @Autowired
    private GenreService genresService;

    @Autowired
    private LanguageService lngService;

    @Autowired
    private UserRepository userRepository;

    @ModelAttribute("allGenres")
    public List<Genres> allGenres() {
        return genresService.findAll();
    }

    @ModelAttribute("allLanguages")
    public List<Languages> allLanguages() {
        return lngService.findAll();
    }

    @GetMapping
    public String index(Model model, @Param("keyword") String keyword, @ModelAttribute(name = "filterObjects") BookFilter filter, Principal principal) {
        List<Books> booksList = service.adminFindAll(filter.getOrderBy(),
                filter.getMode(),
                filter.getPriceFrom(),
                filter.getPriceTo(),
                filter.getGenreId(),
                filter.getLanguageId(),
                keyword
                );
        model.addAttribute("orderby", filter.getOrderBy());
        model.addAttribute("mode", toggleMode(filter.getMode()));

        model.addAttribute("allBooks", booksList);
        model.addAttribute("allLanguages", lngService.findAll());
        Long userId = userRepository.getId(principal.getName());
        model.addAttribute("userId", userId);


        return "a-book-list";
    }

    @GetMapping("/new")
    public String newBook(Model model) {
        model.addAttribute("book", new BookDTO());
        return "new-book";
    }

    @GetMapping("/upd/{id}")
    public String getOne(@PathVariable(value = "id") Long id, Model model) {
        Books book = service.getOne(id);
        System.out.println(book);
        model.addAttribute("book", book);
        return "update-book";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable(value = "id") Long id, Model model) {
        Books book = service.getOne(id);
        System.out.println(book);
        model.addAttribute("book", book);
        return "a-view";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("book") Books book, BindingResult bindingResult) {
        if (bindingResult.hasErrors())
            return "new-book";

        service.save(book);

        return "redirect:/admin/books";
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable("id") Long id, Books book, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors())
            return "update-book";
        service.save(book);
        return "redirect:/admin/books";
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model) {
        Books book = service.getOne(id);

        service.delete(book);
        return "redirect:/admin/books";
    }

    //____________________________________________


    private String toggleMode(String mode){
        String newMode = "desc";
        if(mode.equals("desc"))
            newMode="asc";
        return newMode;
    }


}
