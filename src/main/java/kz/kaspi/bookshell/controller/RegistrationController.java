package kz.kaspi.bookshell.controller;

import kz.kaspi.bookshell.entities.UserEntity;
import kz.kaspi.bookshell.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RegistrationController {
    @Autowired
    private UserRepository userRepository;


    @GetMapping("/registration")
    public String registration(){
        return "registration";

    }

    @PostMapping("/registration")
    public String addUser(UserEntity userEntity, Model model){
        UserEntity userFromDb = userRepository.findByName(userEntity.getName());

        if(userFromDb != null)
            model.addAttribute("message", "User exists!");
        userEntity.setActive(Boolean.TRUE);
        userEntity.setRoleId(2L);
        userRepository.save(userEntity);

        return "redirect:/login";
    }
}
