package kz.kaspi.bookshell.controller;

import kz.kaspi.bookshell.entities.Books;
import kz.kaspi.bookshell.entities.Orders;
import kz.kaspi.bookshell.repositories.UserRepository;
import kz.kaspi.bookshell.services.BooksService;
import kz.kaspi.bookshell.services.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.List;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

@Controller
@RequestMapping("/basket")
public class BasketController {

    @Autowired
    private OrdersService service;

    @Autowired
    private UserRepository repository;

    @Autowired
    private BooksService booksService;

    @PostMapping("/add/{id}")
    public String addOrder(@PathVariable(value = "id") Long id, Model model, Principal principal){
        Long userId = repository.getId(principal.getName());
        Orders order = new Orders(id, userId ,FALSE);
        service.save(order);
        return "redirect:/books";
    }

    @GetMapping
    public String getOrders(Model model, Principal principal) {
        Long userId = repository.getId(principal.getName());
        model.addAttribute("userId", userId);
        List<Orders> orders = service.getPaidOrders(userId);
        model.addAttribute("orders", orders);

        return "basket";
    }

    @GetMapping("/all")
    public String getAllOrders(Model model, Principal principal) {
        Long userId = repository.getId(principal.getName());
        model.addAttribute("userId", userId);
        List<Orders> orders = service.getOrders(userId);
        model.addAttribute("orders", orders);

        return "all-orders";
    }

    @GetMapping("/buy/{id}")
    public String buy(@PathVariable(value = "id") Long id, Model model){
        Orders order = service.getById(id);
        Long bookId = order.getBookId();

        Books book = booksService.getOne(bookId);
        Long amount = book.getAmount();
        order.setIsPaid(TRUE);
        service.save(order);
        book.setAmount(amount - 1);
        booksService.save(book);

        return "redirect:/basket";
    }

    @GetMapping("/delete/{id}")
    public String deleteOrder(@PathVariable("id") long id, Model model) {
        Orders order = service.getById(id);

        service.delete(order);
        return "redirect:/basket";
    }

}
