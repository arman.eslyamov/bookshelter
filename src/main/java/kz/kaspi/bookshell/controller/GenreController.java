package kz.kaspi.bookshell.controller;

import kz.kaspi.bookshell.entities.Genres;

import kz.kaspi.bookshell.errors.ResourceExistsException;
import kz.kaspi.bookshell.services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/genres")
@PreAuthorize("hasAuthority('ADMIN')")
public class GenreController {

    @Autowired
    private GenreService service;


    @GetMapping
    public String newGenre(Model model, Principal principal){
        model.addAttribute("genre", new Genres());
        model.addAttribute("allGenres", service.findAll());

        return "new-genre";
    }

    @PostMapping
    public String saveGenre(Model model, Genres genres, BindingResult bindingResult) throws ResourceExistsException {
        Genres exist = service.selectByName(genres.getName());
        if(bindingResult.hasErrors()){
            bindingResult.addError(new ObjectError("Genre", "Genre exist"));
        }
        service.save(genres);
        return "redirect:/genres";

    }
}
