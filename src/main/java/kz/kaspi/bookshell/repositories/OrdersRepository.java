package kz.kaspi.bookshell.repositories;

import kz.kaspi.bookshell.entities.Orders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long> {

    @Query("SELECT a from Orders a where a.userId = :id ")
    List<Orders> selectByUser(Long id);

    @Query("SELECT a from Orders a where a.userId = :id and a.isPaid=false ")
    List<Orders> selectByUserAndPaid(Long id);

}
