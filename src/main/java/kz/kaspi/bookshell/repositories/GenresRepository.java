package kz.kaspi.bookshell.repositories;

import kz.kaspi.bookshell.entities.Genres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GenresRepository extends JpaRepository<Genres, Long> {

    @Query("SELECT a from Genres a where lower(a.name) like lower(trim(:name)) ")
    Genres selectByName(String name);


}
