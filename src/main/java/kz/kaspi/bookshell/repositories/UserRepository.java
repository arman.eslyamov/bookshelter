package kz.kaspi.bookshell.repositories;

import kz.kaspi.bookshell.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByName(String name);

    @Query("SELECT a.id from UserEntity a where lower(a.name) like lower(:name) ")
    Long getId(String name);

}
