package kz.kaspi.bookshell.repositories;

import kz.kaspi.bookshell.entities.Books;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BooksRepository extends JpaRepository<Books, Long> {

    @Query("select a from Books a where " +
        "(:genreId is null or a.genreId = :genreId) " +
        "and (:languageId is null or a.languageId = :languageId) " +
        "and (:priceTo is null or a.price <= :priceTo) " +
        "and (:priceFrom is null or a.price >= :priceFrom) " +
        "and (a.amount > 0)")
    List<Books> findAll( @Param("priceFrom") Double priceFrom,
                         @Param("priceTo") Double priceTo,
                         @Param("genreId") Long genreId,
                         @Param("languageId") Long languageId,
                         Sort sort);

    @Query("select a from Books a where " +
            "(:genreId is null or a.genreId = :genreId) " +
            "and (:languageId is null or a.languageId = :languageId) " +
            "and (:priceTo is null or a.price <= :priceTo) " +
            "and (:priceFrom is null or a.price >= :priceFrom)")
    List<Books> adminFindAll( @Param("priceFrom") Double priceFrom,
                         @Param("priceTo") Double priceTo,
                         @Param("genreId") Long genreId,
                         @Param("languageId") Long languageId,
                         Sort sort);

    @Query("SELECT p FROM Books p WHERE lower(p.name) LIKE %:keyword%")
    public List<Books> search(String keyword);


}
