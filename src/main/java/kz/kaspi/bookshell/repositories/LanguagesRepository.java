package kz.kaspi.bookshell.repositories;

import kz.kaspi.bookshell.entities.Languages;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguagesRepository extends JpaRepository<Languages, Long> {

    @Query("SELECT a from Languages a where lower(a.name) like lower(trim(:name)) ")
    Languages selectByName(String name);
}
