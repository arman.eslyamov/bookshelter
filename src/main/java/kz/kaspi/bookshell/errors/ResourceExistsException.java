package kz.kaspi.bookshell.errors;

public class ResourceExistsException extends Throwable{
    public ResourceExistsException(String message){super(message);}
}
