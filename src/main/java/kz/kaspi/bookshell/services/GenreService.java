package kz.kaspi.bookshell.services;

import kz.kaspi.bookshell.entities.Genres;
import kz.kaspi.bookshell.errors.ResourceExistsException;
import kz.kaspi.bookshell.repositories.GenresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreService {
    @Autowired
    private GenresRepository repository;

    public List<Genres> findAll() {
        return repository.findAll();
    }

    public Genres save(Genres genre) throws ResourceExistsException {
        Genres existGenre = repository.selectByName(genre.getName());
        if (genre.getName() == null || genre.getName().isBlank())
            throw new ResourceExistsException("Пустое поле имени");
        if (existGenre != null)
            throw new ResourceExistsException("Существующий жанр");
        genre.setName(genre.getName().trim());
        return repository.save(genre);
    }

    public Genres selectByName(String name){return repository.selectByName(name);}

}
