package kz.kaspi.bookshell.services;

import kz.kaspi.bookshell.entities.Roles;
import kz.kaspi.bookshell.entities.UserEntity;
import kz.kaspi.bookshell.repositories.RolesRepository;
import kz.kaspi.bookshell.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RolesRepository rolesRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserEntity userEntity = userRepository.findByName(username);


        if(userEntity == null)
            throw new UsernameNotFoundException("User " + username + " not found");

        ArrayList<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();

        List<Roles> all = rolesRepository.findAll();

        for(Roles role : all){
            String name = role.getName();
            simpleGrantedAuthorities.add(new SimpleGrantedAuthority(name));
        }


        return new User(userEntity.getName(), userEntity.getPassword(), simpleGrantedAuthorities);
    }
}
