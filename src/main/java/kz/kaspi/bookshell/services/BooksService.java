package kz.kaspi.bookshell.services;

import kz.kaspi.bookshell.entities.Books;
import kz.kaspi.bookshell.repositories.BooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BooksService {
    @Autowired
    private BooksRepository repository;

    public List<Books> findAll(
            String orderby,
            String mode,
            Double priceFrom,
            Double priceTo,
            Long genreId,
            Long languageId,
            String keyword){
        if (keyword != null) {
            return repository.search(keyword);

        }

        return repository.findAll(
                priceFrom,
                priceTo,
                genreId,
                languageId,
                defineMode(orderby, mode));
    }


    public List<Books> adminFindAll(
            String orderby,
            String mode,
            Double priceFrom,
            Double priceTo,
            Long genreId,
            Long languageId,
            String keyword){
        if (keyword != null) {
            return repository.search(keyword);

        }

        return repository.adminFindAll(
                priceFrom,
                priceTo,
                genreId,
                languageId,
                defineMode(orderby, mode));
    }


    private Sort defineMode(String orderby, String mode) {
        Sort sort = Sort.by(orderby);

        if(mode.equals("asc"))
            sort = sort.ascending();
        else
            sort = sort.descending();
        return sort;
    }

    public  Books save(Books books){
        return repository.save(books);
    }

    public Books getOne(Long id) {return repository.getById(id);
    }

    public void delete(Books books) {repository.delete(books);}
}
