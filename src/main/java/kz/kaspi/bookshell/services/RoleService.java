package kz.kaspi.bookshell.services;

import kz.kaspi.bookshell.entities.Roles;
import kz.kaspi.bookshell.repositories.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {
    @Autowired
    private RolesRepository repository;

    List<Roles> findAll(){
        return repository.findAll();
    };
}
