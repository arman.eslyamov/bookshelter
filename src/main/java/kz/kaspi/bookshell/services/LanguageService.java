package kz.kaspi.bookshell.services;

import kz.kaspi.bookshell.entities.Languages;
import kz.kaspi.bookshell.repositories.LanguagesRepository;
import kz.kaspi.bookshell.repositories.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageService {
    @Autowired
    private LanguagesRepository repository;

    public List<Languages> findAll() {
        return repository.findAll();
    }


}
