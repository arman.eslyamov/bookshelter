package kz.kaspi.bookshell.services;

import kz.kaspi.bookshell.entities.Books;
import kz.kaspi.bookshell.entities.Orders;
import kz.kaspi.bookshell.repositories.BooksRepository;
import kz.kaspi.bookshell.repositories.OrdersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrdersService {
    @Autowired
    private OrdersRepository repository;

    public Orders save(Orders order){
        return repository.save(order);
    }

    public List<Orders> findAll(){return repository.findAll();}

    public List<Orders> getOrders(Long id) {return repository.selectByUser(id);}

    public List<Orders> getPaidOrders(Long id) {return repository.selectByUserAndPaid(id);}

    public Orders getById(Long id){return repository.getById(id);}

    public void delete(Orders order){repository.delete(order);}

}
