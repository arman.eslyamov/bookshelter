package kz.kaspi.bookshell.entities;

import javax.persistence.*;

@Entity
@Table(name = "genres")
public class Genres {
    @Id
    @Column(name = "genre_id", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long genreId;

    private String name;

    public Genres() {
    }

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
