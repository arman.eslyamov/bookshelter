package kz.kaspi.bookshell.entities;

import javax.persistence.*;

@Entity
@Table(name = "languages")
public class Languages {
    @Id
    @Column(name = "language_id", insertable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long languageId;

    private String name;

    public Languages() {
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
