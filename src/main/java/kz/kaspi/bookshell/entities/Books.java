package kz.kaspi.bookshell.entities;


import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "books")
public class Books {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", insertable = false, updatable = false)
    private Long id;

    private String name;

    private String author;

    @ManyToOne
    @JoinColumn(name = "genre", referencedColumnName="genre_id", updatable = false, insertable = false)
    private Genres genre;

    @Column(name = "genre")
    private Long genreId;

    @ManyToOne
    @JoinColumn(name = "book_language", referencedColumnName="language_id", updatable = false, insertable = false)
    private Languages bookLanguage;

    @Column(name = "book_language")
    private Long languageId;

    private Double price;


    @Column(name = "publishing_day")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate publishingDate;

    private String description;

    private Long amount;


    public Books() {
    }

    public Books(String name, String author, Long genreId, Long languageId, Double price, LocalDate publishingDate, String description, Long amount) {
        this.name = name;
        this.author = author;
        this.genreId = genreId;
        this.languageId = languageId;
        this.price = price;
        this.publishingDate = publishingDate;
        this.description = description;
        this.amount = amount;
    }

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Genres getGenre() {
        return genre;
    }

    public void setGenre(Genres genre) {
        this.genre = genre;
    }

    public Languages getBookLanguage() {
        return bookLanguage;
    }

    public void setBookLanguage(Languages bookLanguage) {
        this.bookLanguage = bookLanguage;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDate getPublishingDate() {
        return publishingDate;
    }

    public void setPublishingDate(LocalDate publishingDate) {
        this.publishingDate = publishingDate;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
