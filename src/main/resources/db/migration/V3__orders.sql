create TABLE if not EXISTS orders (
    order_id bigserial PRIMARY KEY,
    book_id bigint not null,
    user_id BIGINT not null,
    paid BOOLEAN
);

alter table orders add constraint orders_to_books foreign key (book_id) references books;
alter table orders add constraint orders_to_client foreign key (user_id) references users;