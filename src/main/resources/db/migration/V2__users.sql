create table users (
    user_id bigserial primary key,
    name varchar not null,
    password varchar not null,
    role_id bigint not null,
    active boolean
);

create table roles (
    id bigserial primary key,
    name varchar not null unique
);

insert into roles values
    (1, 'ADMIN'),
    (2, 'USER');

insert into users values
(1, 'admin', 'admin', 1, TRUE ),
(2, 'user', 'user', 2, TRUE );

alter table users add constraint roles_to_roles foreign key (role_id) references roles;