create TABLE if not EXISTS books (
     id bigserial PRIMARY KEY,
     name VARCHAR,
     author VARCHAR,
     genre bigint not NULL,
     book_language bigint not NULL,
     price DOUBLE PRECISION,
     publishing_day date,
     amount bigint,
     description text
);

create table if not exists genres (
    genre_id bigserial primary key,
    name varchar
);

create TABLE if not EXISTS languages (
    language_id bigserial PRIMARY KEY,
    name VARCHAR
);

insert into languages VALUES
    (1, 'Russian'),
    (2, 'English'),
    (3, 'Kazakh');

alter table books add constraint genre_to_genres foreign key (genre) references genres;

alter table books add constraint language_to_language foreign key (book_language) references languages;
